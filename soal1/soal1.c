#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<string.h>
#include<unistd.h>
#include<wait.h>
char user[100];
pthread_t t1, t2;

void* download1 (void *arg)
{   
    //printf("1$$$$$$$$$$$$$\n");
    strcpy(user,getenv("USER"));
    char path[] = "/home/";
    strcat(path, user);
    strcat(path, "/quote");
    char *x[] = {"wget", "-P", path, "https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", NULL};
    execv("/bin/wget", x);  
}  

void* download2 (void *arg)
{   
    //printf("2$$$$$$$$$$$$$\n");
    strcpy(user,getenv("USER"));
    char path[] = "/home/";
    strcat(path, user);
    strcat(path, "/music");
    char *x[] = {"wget", "-P", path, "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", NULL};
    execv("/bin/wget", x);  
}  
        
    

int main()
{
    pthread_create(&(t1), NULL, download1, NULL);
    pthread_create(&(t2), NULL, download2, NULL);
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    int status;
    pid_t child;
    child = fork();
    if(child < 0) exit(EXIT_FAILURE);
    else if(child == 0)
    {
        char path[] = "/home/";
        strcat(path, user);
        strcat(path, "/quote");
        char nama1[100], nama2[100];
        strcpy(nama1, path);
        strcpy(nama2, path);
        strcat(nama1, "/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download");
        strcat(nama2, "/quote.zip");
        char *x[] = {"mv", nama1, nama2};
        execv("/bin/mv", x);
    }
    else
    {

    }
}

