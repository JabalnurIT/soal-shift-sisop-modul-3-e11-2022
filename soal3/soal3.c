#include<ctype.h>
#include<dirent.h>
#include<string.h>
#include<unistd.h>
#include<sys/stat.h>
#include<pthread.h>
#include<stdio.h>
#include<sys/types.h>

int cek(const char *fn){
    struct stat status;
    if(!stat(fn, &status)) return 1;
    return 0;
}


void *exc(void *fn){
    char exist_f[200], dirname[200], cwd[PATH_MAX], hidden_f[200], file[200], hidden[200];
    strcpy(exist_f, fn);
    strcpy(hidden_f, fn);
    char *root = strrchr(hidden_f, '/');
    strcpy(hidden, root);
    if(hidden[1] == '.'){
        strcpy(dirname, "hartakarun/");
        strcat(dirname, "Hidden");
    }else if (strstr(fn, ".") != NULL){
        strcpy(file, fn);
        strtok(file, ".");
        char *token = strtok(NULL, "");
        int i;
        for (i = 0; token[i]; i++){
            token[i] = tolower(token[i]);
        }
        char bener[200], box_temp[200];
        strcpy(bener, "");
        strcpy(box_temp, "");
        int a = strlen(exist_f) - 1;
        int ok = 0, idb = 0;
        while(a >= 0){
            if(exist_f[a] == '.'){
                if(!ok){
                    ok = 1;
                    strcpy(box_temp, bener);
                }else{
                    strcpy(box_temp, bener);
                    break;
                }
            }else if(exist_f[a] == '/') break;
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s%c", bener, exist_f[a]);
            strcpy(bener, buffer1);
            a--;
        }
        int lastt = strlen(box_temp) - 1;
        char answer_path[200] = "";
        for(; lastt >= 0; lastt--){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%c", box_temp[lastt]);
            strcat(answer_path, buffer1);
        }
        strcpy(dirname, "hartakarun/");
        strcat(dirname, answer_path);
    }else{
        strcpy(dirname, "hartakarun/");
        strcat(dirname, "Unknown");
    }
    int exist = cek(exist_f);
    if(!strcmp(dirname, "hartakarun/_")) strcpy(dirname, "hartakarun/__");
    if(exist){
        mkdir(dirname, 0777);
    }else puts("not okay!");
    if(getcwd(cwd, sizeof(cwd)) != NULL){
        char *nama = strrchr(fn, '/');
        char namafile[200];
        strcpy(namafile, cwd);
        strcat(namafile, "/");
        strcat(namafile, dirname);
        strcat(namafile, nama);
        rename(fn, namafile);
    }
}

void takerec(char *x){
    char path[PATH_MAX];
    struct dirent *dp;
    struct stat buffer;
    DIR *dir = opendir(x);
    if(!dir) return;
    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") && strcmp(dp->d_name, "..")){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s/%s", x, dp->d_name);
            strcpy(path, buffer1);
            if(!stat(path, &buffer) && S_ISREG(buffer.st_mode)){
                pthread_t thread;
                int err = pthread_create(&thread, NULL, exc, (void *)path);
                pthread_join(thread, NULL);
            }
            takerec(path);
        }
    } closedir(dir);
}

int main(int argc, char *argv[]){
    char cwd[PATH_MAX];
    if(getcwd(cwd, sizeof(cwd)) != NULL){
        strcat(cwd, "/hartakarun");
        takerec(cwd);
    }
}

