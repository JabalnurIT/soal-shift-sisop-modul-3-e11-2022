# **Laporan Penjelasan Soal Shift Modul 3**
## Sistem Operasi E 2022
### **Kelompok E-11**
**Asisten : Dewangga Dharmawan**  
**Anggota :**
- Jabalnur 5025201241
- Vania Rizky Juliana Wachid 5025201215
- Maula Izza Azizi 5025201104  

## **Daftar Isi Laporan**
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)
- [Kendala](#kendala)

# Soal 1
### **Menggunakan thread untuk download 2 file** ###
```c
void* download1 (void *arg)
{   
    strcpy(user,getenv("USER"));
    char path[] = "/home/";
    strcat(path, user);
    strcat(path, "/quote");
    char *x[] = {"wget", "-P", path, "https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", NULL};
    execv("/bin/wget", x);  
}  

void* download2 (void *arg)
{   
    strcpy(user,getenv("USER"));
    char path[] = "/home/";
    strcat(path, user);
    strcat(path, "/music");
    char *x[] = {"wget", "-P", path, "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", NULL};
    execv("/bin/wget", x);  
}  
```
### **unzip file menggunakan child** ###
```c
    pid_t child;
    child = fork();
    int status;
    char zip_file[100];
    sprintf(zip_file, "%s.zip", dir);
    if (child_id < 0)exit(EXIT_FAILURE);
    if(child_id == 0)
    {
        char* argv[] = {"unzip", "-q","-n", zip_file, "-d", dir, NULL};
        execv("/bin/unzip",argv);
        exit(0);
    }

```
### **Rename file menggunakan child** ###
Saat di wget tidak bisa langsung merubah nama file  
```c
int status;
    pid_t child;
    child = fork();
    if(child < 0) exit(EXIT_FAILURE);
    else if(child == 0)
    {
        char path[] = "/home/";
        strcat(path, user);
        strcat(path, "/quote");
        char nama1[100], nama2[100];
        strcpy(nama1, path);
        strcpy(nama2, path);
        strcat(nama1, "/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download");
        strcat(nama2, "/quote.zip");
        char *x[] = {"mv", nama1, nama2};
        execv("/bin/mv", x);
    }
```
### **Decode seluruh file text dengan base 64** ###
```c
const char b64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int b64invs[] = { 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
	59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 44, 45, 46, 47, 48, 49, 50, 51 };

size_t b64_decoded_size(const char *in)
{
	size_t len;
	size_t ret;
	size_t i;

	if (in == NULL)
		return 0;

	len = strlen(in);
	ret = len / 4 * 3;

	for (i=len; i-->0; ) {
		if (in[i] == '=') {
			ret--;
		} else {
			break;
		}
	}

	return ret;
}

void b64_generate_decode_table()
{
	int    inv[80];
	size_t i;

	memset(inv, -1, sizeof(inv));
	for (i=0; i<sizeof(b64chars)-1; i++) {
		inv[b64chars[i]-43] = i;
	}
}

int b64_isvalidchar(char c)
{
	if (c >= '0' && c <= '9')
		return 1;
	if (c >= 'A' && c <= 'Z')
		return 1;
	if (c >= 'a' && c <= 'z')
		return 1;
	if (c == '+' || c == '/' || c == '=')
		return 1;
	return 0;
}

int b64_decode(const char *in, unsigned char *out, size_t outlen)
{
	size_t len;
	size_t i;
	size_t j;
	int    v;

	if (in == NULL || out == NULL)
		return 0;

	len = strlen(in);
	if (outlen < b64_decoded_size(in) || len % 4 != 0)
		return 0;

	for (i=0; i<len; i++) {
		if (!b64_isvalidchar(in[i])) {
			return 0;
		}
	}

	for (i=0, j=0; i<len; i+=4, j+=3) {
		v = b64invs[in[i]-43];
		v = (v << 6) | b64invs[in[i+1]-43];
		v = in[i+2]=='=' ? v << 6 : (v << 6) | b64invs[in[i+2]-43];
		v = in[i+3]=='=' ? v << 6 : (v << 6) | b64invs[in[i+3]-43];

		out[j] = (v >> 16) & 0xFF;
		if (in[i+2] != '=')
			out[j+1] = (v >> 8) & 0xFF;
		if (in[i+3] != '=')
			out[j+2] = v & 0xFF;
	}

	return 1;
}

void *decode(void *file){
    char *filename, buffer[1024], *decoded, loc[20];
    filename = (char *) file;
    size_t len;
    FILE *fptr;
    fptr = fopen(filename, "r");
    fscanf(fptr, "%s", buffer);
    fclose(fptr);

    len = b64_decoded_size(buffer) + 1;
    decoded = (char*) malloc(len*sizeof(char));

    if(b64_decode(buffer, (unsigned char *) decoded, len)){
        decoded[len] = '\0';
        if(filename[0] == 'm') strcpy(loc, "music.txt");
        else if(filename[0] == 'q') strcpy(loc, "quote.txt");
        fptr = fopen(loc, "a");
        fprintf(fptr, "%s\n", decoded);
        fclose(fptr);
    }
}
```
### **Gunakan thread untuk memindah file** ###
```c
void* pindah1 (void *arg)
{   
    strcpy(user,getenv("USER"));
    char path[] = "/home/";
    strcat(path, user);
    strcat(path, "/hasil");
    char *x[] = {"mv", path, "quote.zip", NULL};
    execv("/bin/mv", x);  
}  

void* pindah2 (void *arg)
{   
    strcpy(user,getenv("USER"));
    char path[] = "/home/";
    strcat(path, user);
    strcat(path, "/hasil");
    char *x[] = {"mv", path, "music.zip", NULL};
    execv("/bin/mv", x);  
}  
```
### **Beri password menggunakan fork** ###
```c
void zip_file(char *dir){
    pid_t child;
    int status;
    char zip_file[100];
    sprintf(zip_file, "%s.zip", dir);
    child = fork();
    if (child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0)
    {
        char* argv[] = {"zip", "-P","mihinomenestvania", "-r", zip_file, dir, NULL};
        execv("/bin/zip",argv);
        exit(0);
    }
    else while ((wait(&status)) > 0);
}
zip_file("hasil");
```
### **unzip lalu beri "no"** ###
```c
void unzip_file(char* dir, char* pass){
    pid_t child_id;
    int status;

    char zip_file[100];
    sprintf(zip_file, "%s.zip", dir);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
	if(pass == NULL){
            char* argv[] = {"unzip", "-q","-n", zip_file, "-d", dir, NULL};
            execv("/bin/unzip",argv);
            exit(0);
        }
	
	else {
            char* argv[] = {"unzip", "-P", pass, zip_file, NULL};
            execv("/bin/unzip",argv);
            exit(0);
	}
    }
    else while ((wait(&status)) > 0);
}

void zip_file(char *dir){
    pid_t child_id;
    int status;

    char zip_file[100];
    sprintf(zip_file, "%s.zip", dir);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        char* argv[] = {"zip", "-P","mihinomenestvania", "-r", zip_file, dir, NULL};
        execv("/bin/zip",argv);
        exit(0);
    }
    else while ((wait(&status)) > 0);
}

unzip_file("hasil.zip", "mihinomenestvania");
create_txt("hasil/no.txt");
zip_file("hasil");

```
# Soal 2
# Soal 3
### **Fungsi Untuk Mengecek Status Struk** ###
```c
int cek(const char *fn){
    struct stat status;
    if(!stat(fn, &status)) return 1;
    return 0;
}
```
### **Fungsi yang Akan Dieksekusi Pada Thread untuk Menampung Nama, Membuat Folder, Dan Memindahkan File** ###
```c
void *exc(void *fn){
    char exist_f[200], dirname[200], cwd[PATH_MAX], hidden_f[200], file[200], hidden[200];
    strcpy(exist_f, fn);
    strcpy(hidden_f, fn);
    char *root = strrchr(hidden_f, '/');
    strcpy(hidden, root);
    if(hidden[1] == '.'){
        strcpy(dirname, "hartakarun/");
        strcat(dirname, "Hidden");
    }else if (strstr(fn, ".") != NULL){
        strcpy(file, fn);
        strtok(file, ".");
        char *token = strtok(NULL, "");
        int i;
        for (i = 0; token[i]; i++){
            token[i] = tolower(token[i]);
        }
        char bener[200], box_temp[200];
        strcpy(bener, "");
        strcpy(box_temp, "");
        int a = strlen(exist_f) - 1;
        int ok = 0, idb = 0;
        while(a >= 0){
            if(exist_f[a] == '.'){
                if(!ok){
                    ok = 1;
                    strcpy(box_temp, bener);
                }else{
                    strcpy(box_temp, bener);
                    break;
                }
            }else if(exist_f[a] == '/') break;
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s%c", bener, exist_f[a]);
            strcpy(bener, buffer1);
            a--;
        }
        int lastt = strlen(box_temp) - 1;
        char answer_path[200] = "";
        for(; lastt >= 0; lastt--){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%c", box_temp[lastt]);
            strcat(answer_path, buffer1);
        }
        strcpy(dirname, "hartakarun/");
        strcat(dirname, answer_path);
    }else{
        strcpy(dirname, "hartakarun/");
        strcat(dirname, "Unknown");
    }
    int exist = cek(exist_f);
    if(!strcmp(dirname, "hartakarun/_")) strcpy(dirname, "hartakarun/__");
    if(exist){
        mkdir(dirname, 0777);
    }else puts("not okay!");
    if(getcwd(cwd, sizeof(cwd)) != NULL){
        char *nama = strrchr(fn, '/');
        char namafile[200];
        strcpy(namafile, cwd);
        strcat(namafile, "/");
        strcat(namafile, dirname);
        strcat(namafile, nama);
        rename(fn, namafile);
    }
}
```
### **Fungsi Untuk Menjalankan Thread** ###
```c
void takerec(char *x){
    char path[PATH_MAX];
    struct dirent *dp;
    struct stat buffer;
    DIR *dir = opendir(x);
    if(!dir) return;
    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") && strcmp(dp->d_name, "..")){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s/%s", x, dp->d_name);
            strcpy(path, buffer1);
            if(!stat(path, &buffer) && S_ISREG(buffer.st_mode)){
                pthread_t thread;
                int err = pthread_create(&thread, NULL, exc, (void *)path);
                pthread_join(thread, NULL);
            }
            takerec(path);
        }
    } closedir(dir);
}
```
### **Fungsi Main untuk menjalankan Fungsi Thread** ###
```c
int main(int argc, char *argv[]){
    char cwd[PATH_MAX];
    if(getcwd(cwd, sizeof(cwd)) != NULL){
        strcat(cwd, "/hartakarun");
        takerec(cwd);
    }
}
```
### **Fitur untuk menjalankan Client** ###
```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080
  
int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char hello[20];
    fgets(hello, 20, stdin);
    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }
    pid_t weap_ch;
    weap_ch = fork();
    if(weap_ch < 0){
        exit(EXIT_FAILURE);
    }
    if(!weap_ch) execl("/usr/bin/zip", "zip", "-r", "hartakarun", "../hartakarun", "-q", NULL);
    send(sock , hello , strlen(hello) , 0 );
    valread = read( sock , buffer, 1024);
    printf("%s\n",buffer );
    return 0;
}
```
### **Fungsi Untuk Menjalankan Server** ###
```c
#include <stdio.h>
#include<dirent.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include<pthread.h>
#define PORT 8080

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "";
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    valread = read( new_socket , buffer, 1024);
    if(!strcmp(buffer, "send hartakarun.zip")){
        char cwd[PATH_MAX];
        getcwd(cwd, sizeof(cwd));
        int d = 0;
        while(cwd[d] != '\0') d++;
        char ini_path[200];
        strncpy (ini_path, cwd, d - 7);
        char goalpath[200], startpath[200];
        strcpy(startpath, ini_path);
        strcpy(goalpath, ini_path);
        strcat(startpath, "/Client/hartakarun.zip");
        strcat(goalpath, "/Server/hartakarun.zip");
        rename(startpath, goalpath);
    }
    send(new_socket , hello , strlen(hello) , 0 );
    return 0;
}

```
